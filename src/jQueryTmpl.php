<?php

namespace jossmp\jQueryTmpl;

class jQueryTmpl
{
    public function __construct($tokenizer = NULL, $parser = NULL)
    {
        if ($tokenizer instanceof \jossmp\jQueryTmpl\Tokenizer) {
            $this->_tokenizer = $tokenizer;
        } else {
            $this->_tokenizer = (new \jossmp\jQueryTmpl\Tokenizer\Factory())->create();
        }
        if ($parser instanceof \jossmp\jQueryTmpl\Parser) {
            $this->_parser = $parser;
        } else {
            $this->_parser = (new \jossmp\jQueryTmpl\Parser\Factory())->create();
        }
        $this->_outputBuffer = '';
    }

    public function getHtml()
    {
        $ob = $this->_outputBuffer;
        $this->_outputBuffer = '';
        return $ob;
    }

    public function renderHtml()
    {
        echo $this->getHtml();
        return $this;
    }

    public function getTemplate($name)
    {
        return (isset($this->_markup[$name])) ? $this->_markup[$name] : NULL;
    }
    public function template_from_file($name, $file)
    {
        $jQueryTmpl_Markup_Factory = new \jossmp\jQueryTmpl\Markup\Factory();
        return $this->template($name, $jQueryTmpl_Markup_Factory->createFromFile($file));
    }

    public function template($name, $markup)
    {
        if ($markup instanceof \jossmp\jQueryTmpl\Markup) {
            $this->_markup[$name] = $markup;
            $this->_compiledTemplates[$name] = $this->_compileTemplate($markup);
            return $this;
        } else if (is_string($markup)) {
            $jQueryTmpl_Markup_Factory = new \jossmp\jQueryTmpl\Markup\Factory();
            $markup = $jQueryTmpl_Markup_Factory->createFromString($markup);

            $this->_markup[$name] = $markup;
            $this->_compiledTemplates[$name] = $this->_compileTemplate($markup);
            return $this;
        } else {
            throw new \jossmp\jQueryTmpl\Exception('Invalid data type');
        }
    }

    public function tmpl($nameOrMarkup, $data)
    {
        if ($nameOrMarkup instanceof \jossmp\jQueryTmpl\Markup) {
            $elements = $this->_compileTemplate($nameOrMarkup);
        } else {
            $elements = $this->_compiledTemplates[$nameOrMarkup];
        }

        if (!($data instanceof \jossmp\jQueryTmpl\Data)) {
            $jQueryTmpl_Data_Factory = new \jossmp\jQueryTmpl\Data\Factory();
            if ($data instanceof \stdClass) {
                $data = $jQueryTmpl_Data_Factory->createFromStdClass($data);
            } else if (is_array($data)) {
                $data = $jQueryTmpl_Data_Factory->createFromArray($data);
            } else if (is_string($data)) {
                $data = $jQueryTmpl_Data_Factory->createFromJson($data);
            }
        }

        if (!empty($elements)) {
            $this->_renderElements(
                $elements,
                $data
            );
        }

        return $this;
    }

    private function _compileTemplate(\jossmp\jQueryTmpl\Markup $markup)
    {
        return $this->_parser->parse(
            $this->_tokenizer->tokenize(
                $markup->getTemplate()
            )
        );
    }

    private function _renderElements(array $elements, \jossmp\jQueryTmpl\Data $data)
    {
        foreach ($elements as $element) {
            $this->_outputBuffer .= $element
                ->setData($data)
                ->setCompiledTemplates($this->_compiledTemplates)
                ->render();
        }
    }
}
